https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

curl -LO https://dl.k8s.io/release/v1.22.0/bin/linux/amd64/kubectl
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
chmod +x kubectl
mkdir -p ~/.local/bin/kubectl
mv ./kubectl ~/.local/bin/kubectl
# and then add ~/.local/bin/kubectl to $PATH
kubectl version --client
=================================

ARGO CD CLI:
https://github.com/argoproj/argo-cd/releases

https://www.youtube.com/watch?v=vSnVhJkyJBw&t=241s

kubectl create namespace argocd

kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.1.0-rc3/manifests/install.yaml
kubectl  -n argocd get all
kubectl -n argocd edit svc argocd-server( to change the TYpe to Node port for argocd-server service)

[ec2-user@ip-172-31-8-159 ~]$ kubectl  -n argocd get all
NAME                                      READY   STATUS    RESTARTS   AGE
pod/argocd-application-controller-0       1/1     Running   0          63m
pod/argocd-dex-server-6775cd8fbb-77lnl    1/1     Running   0          63m
pod/argocd-redis-74d8c6db65-kc9sc         1/1     Running   0          63m
pod/argocd-repo-server-56db565fbb-jxjgg   1/1     Running   0          63m
pod/argocd-server-7f995b577f-km879        1/1     Running   0          63m

NAME                            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
service/argocd-dex-server       ClusterIP   10.100.135.130   <none>        5556/TCP,5557/TCP,5558/TCP   63m
service/argocd-metrics          ClusterIP   10.100.62.162    <none>        8082/TCP                     63m
service/argocd-redis            ClusterIP   10.100.146.55    <none>        6379/TCP                     63m
service/argocd-repo-server      ClusterIP   10.100.200.13    <none>        8081/TCP,8084/TCP            63m
service/argocd-server           NodePort    10.100.49.30     <none>        80:30684/TCP,443:31479/TCP   63m
service/argocd-server-metrics   ClusterIP   10.100.24.36     <none>        8083/TCP                     63m

NAME                                 READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/argocd-dex-server    1/1     1            1           63m
deployment.apps/argocd-redis         1/1     1            1           63m
deployment.apps/argocd-repo-server   1/1     1            1           63m
deployment.apps/argocd-server        1/1     1            1           63m

NAME                                            DESIRED   CURRENT   READY   AGE
replicaset.apps/argocd-dex-server-6775cd8fbb    1         1         1       63m
replicaset.apps/argocd-redis-74d8c6db65         1         1         1       63m
replicaset.apps/argocd-repo-server-56db565fbb   1         1         1       63m
replicaset.apps/argocd-server-7f995b577f        1         1         1       63m

NAME                                             READY   AGE
statefulset.apps/argocd-application-controller   1/1     63m

Login into the ARGO CD.
URL: 3.65.25.2:30684
Argo cd- user name:admin
service argocd-server is mapped to 30684

to get the password:TqmXhtrH5mki9h9V

kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
https://stackoverflow.com/questions/68297354/what-is-the-default-password-of-argocd

https://gitlab.com/myleraghu/myproject/-/tree/main/ARGO-CD

https://github.com/justmeandopensource/argocd-demo

ARGO-CD CLI:https:/https://www.youtube.com/watch?v=e9ci_ISVCeM
wget https://github.com/argoproj/argo-cd/releases/download/v2.1.0-rc3/argocd-linux-amd64 -0 argocd
chmod +x argocd
sudo mv argocd /usr/local/bin
argocd help


argocd repo add https://gitlab.com/justmeandopensource/argocd-demo --insecure-skip-server-verification

Create application using ARGO CD UI and deploy it.

To check application:
http://3.65.25.2:32339/
Serivce(nginx is NodePort with port 32339)

D:\Terraform\K8-deployments>kubectl get all
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.100.0.1   <none>        443/TCP   123m

D:\Terraform\K8-deployments>kubectl get all
NAME                         READY   STATUS    RESTARTS   AGE
pod/nginx-6799fc88d8-xnmqw   1/1     Running   0          18s

NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/kubernetes   ClusterIP   10.100.0.1      <none>        443/TCP        124m
service/nginx        NodePort    10.100.38.210   <none>        80:32339/TCP   19s

NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx   1/1     1            1           20s

NAME                               DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-6799fc88d8   1         1         1       20s

D:\Terraform\K8-deployments>kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
nginx-6799fc88d8-xnmqw   1/1     Running   0          32s

D:\Terraform\K8-deployments>kubectl get pods -o wide
NAME                     READY   STATUS    RESTARTS   AGE   IP             NODE                                             NOMINATED NODE   READINESS GATES
nginx-6799fc88d8-xnmqw   1/1     Running   0          69s   172.31.4.175   ip-172-31-13-255.eu-central-1.compute.internal   <none>           <none>

D:\Terraform\K8-deployments>kubectl get nodes -o wide
NAME                                             STATUS   ROLES    AGE    VERSION              INTERNAL-IP     EXTERNAL-IP   OS-IMAGE         KERNEL-VERSION                CONTAINER-RUNTIME
ip-172-31-13-255.eu-central-1.compute.internal   Ready    <none>   123m   v1.20.4-eks-6b7464   172.31.13.255   3.65.25.2     Amazon Linux 2   5.4.129-63.229.amzn2.x86_64   docker://19.3.13


ARGO-CD deployment using Manifest files- Declarative Setup
kubectl apply -f argocd-sample.yaml
===================================
https://argoproj.github.io/argo-cd/operator-manual/declarative-setup/

apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: nginx
  namespace: argocd
spec:
  project: default
  source:
    repoURL: https://github.com/justmeandopensource/argocd-demo.git
    targetRevision: HEAD
    path: yamls
  destination:
    server: https://kubernetes.default.svc
    namespace: nginx


apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: nginx
  namespace: argocd
spec:
  project: default
  source:
    repoURL: https://github.com/argoproj/argocd-example-apps.git
    targetRevision: HEAD
    path: guestbook
  destination:
    server: https://kubernetes.default.svc
    namespace: guestbook

=========================================
ARGO-CD deployment using helm charts

https://mohitgoyal.co/2021/05/03/deploy-helm-charts-on-kubernetes-clusters-with-argo-cd/


Bitnami Charts
This is the Helm repository for Bitnami Charts.
Follow the steps below to start deploying any of them in your Kubernetes cluster. More info at https://github.com/bitnami/charts

$ helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm search repo bitnami
$ helm install my-release bitnami/chart-name

helm install my-release bitnami/grafana

NOTES:
** Please be patient while the chart is being deployed **

1. Get the application URL by running these commands:
    echo "Browse to http://127.0.0.1:8080"
    kubectl port-forward svc/my-release-grafana 8080:3000 &

2. Get the admin credentials:

    echo "User: admin"
    echo "Password: $(kubectl get secret my-release-grafana-admin --namespace default -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 --decode)"
[ec2-user@ip-172-31-8-159 ~]$ kubectl port-forward svc/my-release-grafana 8080:3000
Forwarding from 127.0.0.1:8080 -> 3000
Forwarding from [::1]:8080 -> 3000

^C[ec2-user@ip-172-31-8-159 ~]$ kubectl port-forward svc/my-release-grafana 8080:3000
Forwarding from 127.0.0.1:8080 -> 3000
Forwarding from [::1]:8080 -> 3000



[ec2-user@ip-172-31-8-159 ~]$ kubectl get all
NAME                                      READY   STATUS    RESTARTS   AGE
pod/my-release-1-nginx-5589f4964f-g4pzg   1/1     Running   0          6m31s
pod/my-release-grafana-86dc7d6ff7-v94tr   1/1     Running   0          17m

NAME                         TYPE           CLUSTER-IP       EXTERNAL-IP                                                                  PORT(S)          AGE
service/kubernetes           ClusterIP      10.100.0.1       <none>                                                                       443/TCP          3h18m
service/my-release-1-nginx   LoadBalancer   10.100.214.190   a0d39777f08c042d18bfdd0b1bf5b3a1-1237731865.eu-central-1.elb.amazonaws.com   80:30735/TCP     6m31s
service/my-release-grafana   NodePort       10.100.145.32    <none>                                                                       3000:31443/TCP   17m




    HELM_VERSION: "3.2.2"
    HELM_REPO_NAME: "argo"
    HELM_REPO_URL: "https://argoproj.github.io/argo-helm"
    HELM_CHART_NAME: "argo-cd"

D:\Telefonica\k8s-cluster\eksctl\cluster-template.prod.yaml
pVL6hG3n76Hp9S-P


argocd app create spring-petclinic --repo https://github.com/anthonyvetter/argocd-getting-started.git --path . --dest-server $MINIKUBE_IP --dest-namespace default

helm:
 templates
 charts.yaml
 values.yaml

Raghavendr@63888

"arn:aws:iam::236859812685:role/AdminsFullaccess",
 "arn:aws:iam::842665946605:role/AdminsFullaccess"



========================
HELM


wget https://github.com/argoproj/argo-cd/releases/download/v2.1.0-rc3/argocd-linux-amd64 -0 argocd
chmod +x argocd
sudo mv argocd /usr/local/bin
argocd help

argocd login <argocd-servername>

# Add a private Git repository via HTTPS using username/password without verifying the server's TLS certificate
 argocd repo add https://gitlab.com/myleraghu/myproject.git --username myleraghavendra@gmail.com --password Myle@63888 --insecure-skip-server-verification

k config set-context --current --namespace argocd



kubectl get secret --namespace monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode
