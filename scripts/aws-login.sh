#!/bin/bash
set -e

ROLE_ARN=${AWS_ROLE_ARN};


export AWS_ACCOUNT_ID=$(echo ${ROLE_ARN} | grep -E -o "[0-9]{12}")

# use infrastructure role (overwrite role name)
export ROLE_ARN=$(echo ${ROLE_ARN} | sed -E "s|/.*|/ecs-instance-role-test-web|")

echo "Assuming Role: ${ROLE_ARN}"
CREDENTIALS=$(aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name ${CI_PROJECT_NAME}-ci --output json --query 'Credentials')

export AWS_ACCESS_KEY_ID=$(echo $CREDENTIALS | jq -r '.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo $CREDENTIALS | jq -r '.SecretAccessKey')
export AWS_SESSION_TOKEN=$(echo $CREDENTIALS | jq -r '.SessionToken')
