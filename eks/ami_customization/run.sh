#!/usr/bin/env bash
set -e

#
# NOTE: JQ, Packer and make need to be installed!
# $1.. - K8s Versions to build for (1.14 1.15 ..)
#
# Example: $ ./coms-customize-packer-config.sh 1.14 1.15

EKS_PACKER_IAM_PROFILE="eks-packer-ami"
SSM_TETRA_CERTIFICATE_PATH="/tetra_proxy/ca.crt"
SHARE_WITH_AWS_ACCOUNTS="842665946605"
BUILD_VPC="vpc-026d749110a9b36d8"
BUILD_SUBNET="subnet-02d73d75676e3a718"
DOCKER_MAX_LOG_FILES="1"
DOCKER_MAX_LOG_SIZE="512m"
KMS_KEY_ID="d9c963ab-094c-4d77-913d-76254b4577be"

CUR_PATH=`pwd`
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# helpers
. $SCRIPTPATH/helpers.sh

# clone git project
PACKER_PATH=$($SCRIPTPATH/0.git-clone.sh)
echo "Cloned amazon-eks-ami project here: $PACKER_PATH"

# backup packer-config
cp $PACKER_PATH/eks-worker-al2.json $PACKER_PATH/eks-worker-al2.original.json
cp $PACKER_PATH/files/docker-daemon.json $PACKER_PATH/files/docker-daemon.original.json

# customize packer config
echo "Customizing packer config"
$SCRIPTPATH/1.customize-packer-config.sh $PACKER_PATH/eks-worker-al2.json "$BUILD_VPC" "$BUILD_SUBNET" "$EKS_PACKER_IAM_PROFILE" "$SSM_TETRA_CERTIFICATE_PATH" "$KMS_KEY_ID" "$SHARE_WITH_AWS_ACCOUNTS"

# customize docker daemon
echo "Customizing docker config"
$SCRIPTPATH/2.customize-docker-daemon.sh $PACKER_PATH/files/docker-daemon.json "$DOCKER_MAX_LOG_SIZE" "$DOCKER_MAX_LOG_FILES"

# build images
echo "Build EKS images"
$SCRIPTPATH/3.build.sh $SCRIPTPATH/../amazon-eks-ami/eks-worker-al2.json $@

# cleanup
echo "Cleanup"
rm -rf "$PACKER_PATH"
cd "$CUR_PATH"
