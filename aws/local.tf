locals {
  bala_tefzone_a_cidr = "100.64.2.0/23"
  bala_tefzone_c_cidr = "100.64.0.0/24"
  is_dev              = var.environment == "dev" ? 1 : 0
 }
