################################################################################
# General
################################################################################

environment = "dev"

region = "eu-central-1"

name = "coms"

project = "coms-dev"

#tfstate_bucket = "tefde-ccm-coms-dev-tfstate"

#dns_prefix = "coms-dev"

bala_vpc_cidr = "10.170.111.128/26"

bala_vpc_secondary_cidr = "100.64.0.0/22"

bala_vpc_name = "coms_dev_BALA_vpc"
aws_profile_name = "tefde_ccm_coms_dev_deploy"
