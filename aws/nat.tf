module "nat" {
  source       = "./modules/nat"
  name         = "${var.name}_${var.environment}_bala_tefzone_b"
  route_tables = module.subnets_tefzone_bala.route_table
  subnets      = module.subnets_tefzone_c.subnet
}
