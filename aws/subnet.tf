module "subnets_tefzone_a" {
  source = "./modules/subnet"

  project     = "${var.name}_bala"
  name        = "tefzone_a_az"
  environment = var.environment
  vpc_id      = aws_vpc.bala_vpc.id
  cidr        = local.bala_tefzone_a_cidr

  tags = {
    "CIDR"                                              = "private"
    "tef-security-zone"                                 = "A"
    "kubernetes.io/cluster/coms-${var.environment}-eks" = "shared"
  }
}

module "subnets_tefzone_bala" {
  source = "./modules/subnet"

  project     = "${var.name}_bala"
  name        = "tefzone_b_az"
  environment = var.environment
  vpc_id      = aws_vpc.bala_vpc.id
  cidr        = var.bala_vpc_cidr

  tags = {
    "CIDR"                                              = "BALA"
    "tef-security-zone"                                 = "B"
    "kubernetes.io/cluster/coms-${var.environment}-eks" = "shared"
    "kubernetes.io/role/internal-elb"                   = "1"
  }
}

module "subnets_tefzone_c" {
  source = "./modules/subnet"

  project     = "${var.name}_bala"
  name        = "tefzone_c_az"
  environment = var.environment
  vpc_id      = aws_vpc.bala_vpc.id
  cidr        = local.bala_tefzone_c_cidr

  tags = {
    "CIDR"              = "private"
    "tef-security-zone" = "C"
  }
}
