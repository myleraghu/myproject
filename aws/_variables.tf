################################################################################
# General
################################################################################

variable "environment" {
}

variable "region" {
  default = "eu-central-1"
}

variable "name" {
}

variable "project" {
}

#variable "tfstate_bucket" {
#}

#variable "dns_prefix" {
#}

variable "bala_vpc_cidr" {
}

variable "bala_vpc_secondary_cidr" {
}

variable "bala_vpc_name" {
}

variable "aws_profile_name" {
}

variable "amazon_linux_2_ami" {
  default = "ami-043097594a7df80ec"
}

variable "tag_os_amazon_linux_2" {
  default = "AmazonLinux2"
}


variable "devlan_cidr" {
  default = "0.0.0.0/0"
}

variable "shared_service_cidr" {
  default = "0.0.0.0/0"
}

variable "vpn_cidr" {
  default = "0.0.0.0/0"
}
