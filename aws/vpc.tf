################################################################################
# VPCs
################################################################################

resource "aws_vpc" "bala_vpc" {
  cidr_block = var.bala_vpc_cidr

  tags = {
    "Name"                                              = var.bala_vpc_name
    "Environment"                                       = var.environment
    "kubernetes.io/cluster/coms-${var.environment}-eks" = "shared"
    "CIDR"                                              = "BALA-TETS"
    "Project"                                           = var.project
  }
}

resource "aws_vpc_ipv4_cidr_block_association" "bala_vpc_secondary_cidr" {
  vpc_id     = aws_vpc.bala_vpc.id
  cidr_block = var.bala_vpc_secondary_cidr
}

################################################################################
# DHCP Options
################################################################################

#resource "aws_vpc_dhcp_options" "dhcp_bala" {
#  domain_name         = "${var.region}.compute.internal"
#  domain_name_servers = compact(module.dns_forwarder.private_ips)

#  tags = {
#    Name = "${var.project}-bala"
#  }
#}

#resource "aws_vpc_dhcp_options_association" "dhcp_bala" {
#  vpc_id          = aws_vpc.bala_vpc.id
#  dhcp_options_id = aws_vpc_dhcp_options.dhcp_bala.id
#}

################################################################################
# Flow Logs
################################################################################

resource "aws_flow_log" "bala_vpc" {
  log_destination          = aws_cloudwatch_log_group.bala.arn
  log_destination_type     = "cloud-watch-logs"
  iam_role_arn             = aws_iam_role.flow_logs_bala.arn
  traffic_type             = "ALL"
  vpc_id                   = aws_vpc.bala_vpc.id
  max_aggregation_interval = 600
}

resource "aws_cloudwatch_log_group" "bala" {
  name              = "flow-logs-${var.project}"
  retention_in_days = 30
}

resource "aws_iam_role" "flow_logs_bala" {
  name = "flow-logs-${var.project}"

  assume_role_policy = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "vpc-flow-logs.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  }
  POLICY
}

resource "aws_iam_role_policy" "flow_logs_bala" {
  name = "logs"
  role = aws_iam_role.flow_logs_bala.id

  policy = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
          "logs:DescribeLogGroups",
          "logs:DescribeLogStreams"
        ],
        "Effect": "Allow",
        "Resource": "*"
      }
    ]
  }
  POLICY
}
