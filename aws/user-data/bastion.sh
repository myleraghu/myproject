#!/bin/bash


export AWS_RETRY_MODE="standard"
export AWS_MAX_ATTEMPTS="3"


yum update -y

# install eksctl
curl -LO https://github.com/weaveworks/eksctl/releases/download/${eksctl_verison}/eksctl_Linux_amd64.tar.gz
tar xzf eksctl_Linux_amd64.tar.gz; mv eksctl /usr/local/bin/
rm eksctl_Linux_amd64.tar.gz

# install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/v${kubectl_version}/bin/linux/amd64/kubectl
chmod +x kubectl; mv kubectl /usr/local/bin/

# install helm
curl -LO https://get.helm.sh/helm-v${helm_verison}-linux-amd64.tar.gz
tar xzvf helm-v${helm_verison}-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/
rm -rf linux-amd64/ helm*

USER_DIR="/home/ec2-user"
echo "alias k='kubectl \$@'" >> $USER_DIR/.bashrc
#export KUBECONFIG=$USER_DIR/.kube/config

#sudo -u ec2-user aws configure set region ${region}
#sudo -u ec2-user aws eks update-kubeconfig --name coms-${environment}-eks --alias ${environment}

#sudo -u ec2-user kubectl config use-context ${environment}
#sudo -u ec2-user kubectl config set-context --current --namespace monitoring
