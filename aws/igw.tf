module "igw" {
  source       = "./modules/igw"
  vpc_id       = aws_vpc.bala_vpc.id
  name         = "${var.name}_${var.environment}_bala_tefzone_c"
  route_tables = module.subnets_tefzone_c.route_table
}
