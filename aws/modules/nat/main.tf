resource "aws_nat_gateway" "this" {
  count = length(var.subnets)

  allocation_id = element(aws_eip.this.*.id, count.index)
  subnet_id     = element(var.subnets, count.index)

  tags = {
    Name = "${var.name}_${count.index}"
  }
}

resource "aws_eip" "this" {
  count = length(var.subnets)
  vpc   = true

  tags = {
    Name = "${var.name}_${count.index}"
  }
}

resource "aws_route" "this" {
  count                  = length(var.route_tables)
  route_table_id         = element(var.route_tables, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.this.*.id, count.index)
}
