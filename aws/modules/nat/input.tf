variable "name" {}

variable "route_tables" {
  type    = list(string)
  default = []
}

variable "subnets" {
  type = list(string)
}
