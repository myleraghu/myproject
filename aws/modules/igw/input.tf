variable "name" {}

variable "vpc_id" {}

variable "route_tables" {
  type    = list(string)
  default = []
}
