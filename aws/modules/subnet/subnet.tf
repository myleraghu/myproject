data "aws_availability_zones" "available" {}

resource "aws_subnet" "this" {
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = var.vpc_id
  availability_zone = element(data.aws_availability_zones.available.names, count.index)
  cidr_block        = cidrsubnet(var.cidr, 2, count.index)

  tags = merge(
    var.tags,
    {
      Name = "${var.project}_${var.name}_${count.index}_${var.environment}"
    }
  )

  # ignore changes to tags, because EKS will manage them
  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

resource "aws_route_table" "this" {
  count  = length(data.aws_availability_zones.available.names)
  vpc_id = var.vpc_id

  tags = merge(
    var.tags,
    {
      Name = "${var.project}_${var.name}_${count.index}_${var.environment}"
    }
  )
}

resource "aws_route_table_association" "this" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = element(aws_subnet.this.*.id, count.index)
  route_table_id = element(aws_route_table.this.*.id, count.index)
}
