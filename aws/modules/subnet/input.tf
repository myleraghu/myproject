variable "name" {}

variable "environment" {}

variable "vpc_id" {}

variable "cidr" {}

variable "project" {}

variable "tags" {
  type    = map(string)
  default = {}
}
