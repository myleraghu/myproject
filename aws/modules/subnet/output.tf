output "route_table" {
  value = aws_route_table.this.*.id
}

output "subnet" {
  value = aws_subnet.this.*.id
}

output "cidrs" {
  value = aws_subnet.this.*.cidr_block
}
