variable "name" {}

variable "ami" {}

variable "instance_type" {}

variable "volume_size" {
  default = 8
}

variable "min_size" {
  default = 1
}

variable "max_size" {
  default = 1
}

variable "desired_capacity" {
  default = 1
}

variable "user_data" {}

variable "iam_policy" {}

variable "key_name" {
  default = null
}

variable "vpc_id" {}

variable "subnets" {
  type = list(string)
}

variable "security_group_rules" {
  type    = map(map(string))
  default = {}
}

variable "use_spot" {
  default = false
}

variable "instance_types_spot" {
  default = []
}

variable "additional_tags" {
  type        = map(string)
  description = "Map of additional tags which will propagate to the instance"
  default     = {}
}
