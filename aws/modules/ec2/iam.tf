resource "aws_iam_role" "self" {
  name = var.name

  assume_role_policy = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "",
        "Effect": "Allow",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  }
  POLICY
}

resource "aws_iam_instance_profile" "self" {
  name = var.name
  path = "/"
  role = aws_iam_role.self.id
}

resource "aws_iam_role_policy" "inline" {
  name = var.name
  role = aws_iam_role.self.id

  policy = var.iam_policy
}

resource "aws_iam_role_policy_attachment" "cw_agent" {
  role       = aws_iam_role.self.id
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentAdminPolicy"
}

resource "aws_iam_role_policy" "eks_node_ssm_inventory" {
  name = "ssm-inventory"
  role = aws_iam_role.self.id

  policy = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "ssm:DescribeAssociation",
          "ssm:ListAssociations",
          "ssm:ListInstanceAssociations",
          "ssm:PutInventory",
          "ssm:PutComplianceItems",
          "ssm:UpdateAssociationStatus",
          "ssm:UpdateInstanceAssociationStatus",
          "ssm:UpdateInstanceInformation",
          "ssm:GetDocument",
          "ssm:DescribeDocument",
          "ssm:GetManifest"
        ],
        "Resource": "*"
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2messages:AcknowledgeMessage",
          "ec2messages:DeleteMessage",
          "ec2messages:FailMessage",
          "ec2messages:GetEndpoint",
          "ec2messages:GetMessages",
          "ec2messages:SendReply"
        ],
        "Resource": "*"
      }
    ]
  }
  POLICY
}
