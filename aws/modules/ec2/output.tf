output "instance_role_name" {
  value = aws_iam_role.self.name
}

output "instance_role_arn" {
  value = aws_iam_role.self.arn
}

output "security_group_id" {
  value = aws_security_group.self.id
}
