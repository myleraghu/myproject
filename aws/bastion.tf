module "bastion" {
  source = "./modules/ec2"

  name                = "bastion"
  instance_type       = "t3.nano"
  ami                 = var.amazon_linux_2_ami
  key_name            = "gitlab-ci-new"
  vpc_id              = aws_vpc.bala_vpc.id
  subnets             = module.subnets_tefzone_c.subnet
  use_spot            = true
  instance_types_spot = ["t3.small", "t3a.small"]

  security_group_rules = local.is_dev == 1 ? {} : {
    rule1 : { port : 22, cidr : var.devlan_cidr, description : "DevLan" },
    rule2 : { port : 22, cidr : var.vpn_cidr, description : "VPN" }
    rule3 : { port : 22, cidr : var.shared_service_cidr, description : "Shared Services" }
  }

  additional_tags = {
    Module              = "bastion"
    OS                  = var.tag_os_amazon_linux_2
    "tef-security-zone" = "B"
  }

  iam_policy = file("${path.root}/iam/bastion.json")
  user_data  = data.template_file.bastion_user_data.rendered
}

data "template_file" "bastion_user_data" {
  template = file("${path.root}/user-data/bastion.sh")

  vars = {
    region          = var.region
    environment     = var.environment
    #r53_record_name = "bastion.${var.coms_domain_name}."
    #r53_hosted_zone = aws_route53_zone.bala_zone.zone_id
    kubectl_version = "1.21.3"
    eksctl_verison  = "0.59.0"
    helm_verison    = "3.6.3"
  }
}
